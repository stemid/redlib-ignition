data "ignition_file" "environment" {
  path = "${var.home}/environment"
  content {
    content = templatefile("${path.module}/templates/environment", {})
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "container_unit" {
  path = "${var.home}/.config/containers/systemd/redlib.container"
  content {
    content = templatefile("${path.module}/templates/redlib.container", {
      home = var.home
      publish_host = var.publish_host
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_config" "config" {
  files = [
    data.ignition_file.environment.rendered,
    data.ignition_file.container_unit.rendered,
  ]
}
